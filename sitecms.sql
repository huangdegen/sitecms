-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2017-12-31 13:54:35
-- 服务器版本： 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tp3`
--

-- --------------------------------------------------------

--
-- 表的结构 `sc_page_article`
--

CREATE TABLE `sc_page_article` (
  `id` int(11) NOT NULL,
  `nid` int(1) NOT NULL DEFAULT '0' COMMENT '节点ID',
  `nid_other` int(1) NOT NULL DEFAULT '0' COMMENT '附属节点ID',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `title_sub` varchar(255) DEFAULT NULL COMMENT '文章副标题',
  `description` text COMMENT '文章概述',
  `thumb` varchar(255) DEFAULT NULL COMMENT '封面图',
  `url` varchar(255) DEFAULT NULL COMMENT '原文链接',
  `content` text COMMENT '内容',
  `author_id` int(1) NOT NULL DEFAULT '0' COMMENT '发布者ID',
  `author` varchar(32) DEFAULT NULL COMMENT '文章作者',
  `source` varchar(255) DEFAULT NULL COMMENT '文章来源',
  `count` int(1) NOT NULL DEFAULT '0' COMMENT '阅读数/点击数',
  `is_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态,1启用,2禁用',
  `is_recommend` tinyint(1) NOT NULL DEFAULT '0' COMMENT '推荐,1推荐,0不推荐',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章库';

-- --------------------------------------------------------

--
-- 表的结构 `sc_page_gallery`
--

CREATE TABLE `sc_page_gallery` (
  `id` int(11) NOT NULL,
  `gid` varchar(32) DEFAULT NULL COMMENT '图册唯一匹配id',
  `title` varchar(255) DEFAULT NULL COMMENT '图片标题',
  `image_src` varchar(255) DEFAULT NULL COMMENT '图片地址',
  `description` text COMMENT '图片描述',
  `order_id` int(11) NOT NULL DEFAULT '0' COMMENT '图片排序'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='图册库';

-- --------------------------------------------------------

--
-- 表的结构 `sc_page_product`
--

CREATE TABLE `sc_page_product` (
  `id` int(11) NOT NULL,
  `nid` int(1) NOT NULL DEFAULT '0' COMMENT '节点ID',
  `nid_other` int(1) NOT NULL DEFAULT '0' COMMENT '附属节点ID',
  `gid` varchar(32) DEFAULT NULL COMMENT '图册唯一匹配id',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `thumb` varchar(255) DEFAULT NULL COMMENT '封面图',
  `dimension` varchar(100) DEFAULT NULL COMMENT '产品规格',
  `code` varchar(32) DEFAULT NULL COMMENT '产品编码|条码',
  `price` varchar(20) NOT NULL DEFAULT '0' COMMENT '价格',
  `unit` varchar(32) DEFAULT NULL COMMENT '产品单位',
  `url` varchar(255) DEFAULT NULL COMMENT '原文链接',
  `description` text COMMENT '描述',
  `content` text COMMENT '内容',
  `author_id` int(1) NOT NULL DEFAULT '0' COMMENT '发布者ID',
  `count` int(1) NOT NULL DEFAULT '0' COMMENT '阅读数/点击数',
  `is_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态,1启用,2禁用',
  `is_recommend` tinyint(1) NOT NULL DEFAULT '0' COMMENT '推荐,1推荐,0不推荐',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产品库';

-- --------------------------------------------------------

--
-- 表的结构 `sc_page_single`
--

CREATE TABLE `sc_page_single` (
  `id` int(11) NOT NULL,
  `nid` int(1) NOT NULL DEFAULT '0' COMMENT '节点ID',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `title_sub` varchar(255) DEFAULT NULL COMMENT '副标题',
  `thumb` varchar(255) DEFAULT NULL COMMENT '封面图',
  `description` text COMMENT '描述',
  `content` text COMMENT '内容'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='单页库';

-- --------------------------------------------------------

--
-- 表的结构 `sc_site_config`
--

CREATE TABLE `sc_site_config` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT '站点名称',
  `title` varchar(255) DEFAULT NULL COMMENT 'SEO标题',
  `keywords` varchar(25) DEFAULT NULL COMMENT 'SEO关键词',
  `description` text COMMENT 'SEO描述',
  `copyright` varchar(255) DEFAULT NULL COMMENT '站点版权',
  `icp` varchar(32) DEFAULT NULL COMMENT 'ICP备案号',
  `statistics` varchar(255) DEFAULT NULL COMMENT '统计代码',
  `phone` varchar(32) DEFAULT NULL COMMENT '联系手机',
  `tel` varchar(32) DEFAULT NULL COMMENT '联系电话',
  `fax` varchar(32) DEFAULT NULL COMMENT '传真号码',
  `email` varchar(32) DEFAULT NULL COMMENT '电子邮箱',
  `qq` varchar(32) NOT NULL DEFAULT '0' COMMENT '客服QQ',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `other` varchar(255) DEFAULT NULL COMMENT '其他'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站点信息配置';

--
-- 转存表中的数据 `sc_site_config`
--

INSERT INTO `sc_site_config` (`id`, `name`, `title`, `keywords`, `description`, `copyright`, `icp`, `statistics`, `phone`, `tel`, `fax`, `email`, `qq`, `address`, `other`) VALUES
(1, 'SiteCMS测试站', 'SiteCMS测试站', 'cms,sitecms', '一款极简的企业cms', 'Copyright 2018 sitecms.cn All rights reserved', '闽ICP备17008220号', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `sc_site_node`
--

CREATE TABLE `sc_site_node` (
  `id` int(11) NOT NULL,
  `pid` int(1) NOT NULL DEFAULT '0' COMMENT '父id',
  `title` varchar(100) DEFAULT NULL COMMENT '名称',
  `title_en` varchar(30) DEFAULT NULL COMMENT '英文名称',
  `subtitle` varchar(100) DEFAULT NULL COMMENT '别名',
  `flag` varchar(100) DEFAULT NULL COMMENT '唯一标识',
  `path` varchar(100) DEFAULT NULL COMMENT '路径',
  `tid` int(1) NOT NULL DEFAULT '1' COMMENT '模型类别',
  `thumb` varchar(100) DEFAULT NULL COMMENT '缩略图',
  `keywords` text COMMENT 'seo关键词',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `url` varchar(255) DEFAULT NULL COMMENT '外链',
  `order_id` int(1) NOT NULL DEFAULT '0' COMMENT '排序',
  `is_show` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示',
  `is_url` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否外链',
  `is_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='导航库';

-- --------------------------------------------------------

--
-- 表的结构 `sc_site_slide`
--

CREATE TABLE `sc_site_slide` (
  `id` int(11) NOT NULL,
  `title` varchar(32) DEFAULT NULL COMMENT '标题',
  `thumb` varchar(255) DEFAULT NULL COMMENT '封面图',
  `url` varchar(255) DEFAULT NULL COMMENT '原文链接',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `is_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态,1启用,0禁用',
  `order_id` int(1) NOT NULL DEFAULT '0' COMMENT '排序号'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='幻灯片库';

-- --------------------------------------------------------

--
-- 表的结构 `sc_site_type`
--

CREATE TABLE `sc_site_type` (
  `id` int(11) NOT NULL,
  `title` varchar(30) DEFAULT NULL COMMENT '标题',
  `flag` varchar(30) DEFAULT NULL COMMENT '标识',
  `is_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='模型库';

--
-- 转存表中的数据 `sc_site_type`
--

INSERT INTO `sc_site_type` (`id`, `title`, `flag`, `is_status`) VALUES
(1, '文章', 'article', 1),
(2, '单页', 'single', 1),
(3, '产品', 'product', 1);

-- --------------------------------------------------------

--
-- 表的结构 `sc_sys_admin`
--

CREATE TABLE `sc_sys_admin` (
  `id` int(11) NOT NULL,
  `group` int(1) NOT NULL COMMENT '权限分组',
  `username` varchar(32) DEFAULT NULL COMMENT '登录名',
  `password` varchar(32) NOT NULL DEFAULT '496ab4cc4f298f70a9639178430da22a' COMMENT '登录密码,默认123456',
  `phone` varchar(32) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(32) DEFAULT NULL COMMENT '联系邮箱',
  `is_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态,1启用,0禁用',
  `login_num` int(11) NOT NULL DEFAULT '0' COMMENT '登录次数'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员列表';

--
-- 转存表中的数据 `sc_sys_admin`
--

INSERT INTO `sc_sys_admin` (`id`, `group`, `username`, `password`, `phone`, `email`, `is_status`, `login_num`) VALUES
(1, 0, 'sitecms', '6f698f2f137a9f1e49a303bd0b1af2dd', '', '', 1, 1),
(2, 0, 'admin', '6f698f2f137a9f1e49a303bd0b1af2dd', '', '', 1, 1);

-- --------------------------------------------------------

--
-- 表的结构 `sc_sys_adminlog`
--

CREATE TABLE `sc_sys_adminlog` (
  `id` int(11) NOT NULL,
  `username` varchar(32) DEFAULT NULL COMMENT '登录名',
  `source` varchar(32) NOT NULL COMMENT '登录IP',
  `create_time` datetime DEFAULT NULL COMMENT '登录时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员登录日志';

-- --------------------------------------------------------

--
-- 表的结构 `sc_sys_config`
--

CREATE TABLE `sc_sys_config` (
  `id` int(11) NOT NULL,
  `name` varchar(32) DEFAULT NULL COMMENT '系统名称',
  `app_name` varchar(32) DEFAULT NULL COMMENT '程序名称',
  `app_version` varchar(10) DEFAULT NULL COMMENT '程序版本',
  `tel` varchar(32) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(32) DEFAULT NULL COMMENT '电子邮箱',
  `qq` varchar(32) DEFAULT NULL COMMENT '客服QQ',
  `website` varchar(32) DEFAULT NULL COMMENT '官方网站',
  `other` varchar(255) DEFAULT NULL COMMENT '其他'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统信息配置';

--
-- 转存表中的数据 `sc_sys_config`
--

INSERT INTO `sc_sys_config` (`id`, `name`, `app_name`, `app_version`, `tel`, `email`, `qq`, `website`, `other`) VALUES
(1, 'siteCMS测试站', '欢迎使用! Sitecms', 'V3.2.3', '', '', '', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `sc_sys_node`
--

CREATE TABLE `sc_sys_node` (
  `id` int(11) NOT NULL,
  `pid` int(1) NOT NULL DEFAULT '0' COMMENT '父级ID',
  `title` varchar(32) DEFAULT NULL COMMENT '名称',
  `path` varchar(255) DEFAULT NULL COMMENT '路径',
  `icon` varchar(32) DEFAULT NULL COMMENT '图标',
  `is_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态,1启用,0禁用',
  `is_system` tinyint(1) NOT NULL DEFAULT '0' COMMENT '系统节点是否管理员可见，1:不可见,0:可见',
  `order_id` int(1) NOT NULL DEFAULT '0' COMMENT '排序号'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统节点';

--
-- 转存表中的数据 `sc_sys_node`
--

INSERT INTO `sc_sys_node` (`id`, `pid`, `title`, `path`, `icon`, `is_status`, `is_system`, `order_id`) VALUES
(1, 0, '内容管理', '#', 'fa fa-desktop', 1, 0, 0),
(2, 0, '站点设置', '#', 'fa fa-cog', 1, 0, 0),
(3, 0, '系统设置', '#', 'fa fa-cogs', 1, 1, 0),
(4, 2, '站点配置', '/siteconf/index', '', 1, 0, 0),
(5, 2, '导航管理', '/sitenode/index', '', 1, 0, 0),
(6, 2, '幻灯片管理', '/slide/index', '', 1, 0, 0),
(7, 2, '管理员管理', '/admin/index', '', 1, 0, 0),
(8, 3, '系统配置', '/sysconf/index', '', 1, 0, 0),
(9, 3, '节点管理', '/sysnode/index', '', 1, 0, 0),
(10, 3, '模型管理', '/type/index', '', 1, 0, 0),
(11, 2, '数据库管理', '/database/index', '', 1, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sc_page_article`
--
ALTER TABLE `sc_page_article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sc_page_gallery`
--
ALTER TABLE `sc_page_gallery`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `sc_page_product`
--
ALTER TABLE `sc_page_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sc_page_single`
--
ALTER TABLE `sc_page_single`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sc_site_config`
--
ALTER TABLE `sc_site_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sc_site_node`
--
ALTER TABLE `sc_site_node`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sc_site_slide`
--
ALTER TABLE `sc_site_slide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sc_site_type`
--
ALTER TABLE `sc_site_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sc_sys_admin`
--
ALTER TABLE `sc_sys_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sc_sys_adminlog`
--
ALTER TABLE `sc_sys_adminlog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sc_sys_config`
--
ALTER TABLE `sc_sys_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sc_sys_node`
--
ALTER TABLE `sc_sys_node`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `sc_page_article`
--
ALTER TABLE `sc_page_article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `sc_page_gallery`
--
ALTER TABLE `sc_page_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `sc_page_product`
--
ALTER TABLE `sc_page_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `sc_page_single`
--
ALTER TABLE `sc_page_single`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `sc_site_config`
--
ALTER TABLE `sc_site_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `sc_site_node`
--
ALTER TABLE `sc_site_node`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `sc_site_slide`
--
ALTER TABLE `sc_site_slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `sc_site_type`
--
ALTER TABLE `sc_site_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `sc_sys_admin`
--
ALTER TABLE `sc_sys_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `sc_sys_adminlog`
--
ALTER TABLE `sc_sys_adminlog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `sc_sys_config`
--
ALTER TABLE `sc_sys_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `sc_sys_node`
--
ALTER TABLE `sc_sys_node`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
