<?php
namespace Admin\Controller;

class IndexController extends BasicController {
    public function index(){
        !session('?admin_id') && $this->redirect('/login');
        //节点列表
        if(session('admin_id') !== '1'){
            $where['is_system'] = array('EQ',0);
        }
        $where['is_status'] = array('EQ',1);
        $node = M('SysNode')->where($where)->select();
        $node = !empty($node) ? formatTreeArr($node) : [];
        $this->assign('node',$node);
        //导航列表
        $category = M('SiteNode')->order('order_id desc')->select();
        foreach ($category as $key => $value) {
            $category[$key]['flag'] = M('SiteType')->where('id',$value['mid'])->getField('flag');
        }
        $category = !empty($category) ? toLevel($category) : [];
        $this->assign('category',$category);
        $this->display();
    }
    public function main(){
        $this -> assign('title', '系统首页');
        $this -> assign('server_time', date('Y年n月j日 H:i:s'));
        $Model = new \Think\Model();
        $_version = $Model->query('select version() as ver');
        $version = array_pop($_version);
        $this -> assign('mysql_ver', $version['ver']);
        $contact = M('SysConfig')->where('id','1')->find();
        $contact['email'] = $contact['email'] ? '<a href="mailto:'.$contact['email'].'">'.$contact['email'].'</a>' : '';
        $contact['website'] = $contact['website'] ? '<a href="'.$contact['website'].'" target="_blank">'.$contact['website'].'</a>' : '';
        $this -> assign('contact', $contact);
        $this->display();
    }
}
