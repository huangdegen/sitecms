<?php
namespace Admin\Controller;
class SysConfController extends BasicController {
    // 定义全局参数
    private $table = 'SysConfig';

    function _initialize(){
        parent::_getNode();
        //得到节点内容
        $this->nodeInfo = M('SysNode')->where(array('id'=>$this->nid))->find();
        $this->assign('nodeInfo', $this->nodeInfo);
    }
    public function index(){
        $view = M($this->table)->where(array('id'=>1))->find();
        $this->assign('view', $view);

        $this->display();
    }
    // 编辑
    public function save(){
        if(IS_AJAX){
            $Model_Data = M($this->table);
            if($Model_Data->create()){
                $Model_Data->where(array('id'=>1))->save() !== false ? $this->success('保存成功!',U('index').'?spm='.$this->spm) : $this->error('保存失败!');
            }else{
                $this->error($Model_Data->getError());
            }
        }else{
            echo "非法操作";
        }
    }
}
