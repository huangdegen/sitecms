<?php
namespace Admin\Controller;
class PageController extends BasicController {
    function _initialize(){
        parent::_getNode();
        //得到节点内容
        $this->category = M('SiteNode')->select();
        $this->category = !empty($this->category) ? toLevel($this->category) : [];
        $this->assign('category', $this->category);
        $nodeInfo = getSelf($this->category,$this->nid);
        $nodeInfo['type'] = M('SiteType')->where(array('id'=>$nodeInfo['tid']))->getField('flag');
        if($nodeInfo['type'] == ''){
            $nodeInfo['type'] = 'article';
        }
        $nodeInfo['children'] = hasChild($this->category,$nodeInfo['id']) ? 1 : 0;
        $this->assign('nodeInfo', $nodeInfo);
    }
    public function index(){
        $p = $_GET['p'] ? $_GET['p'] : 1;
        if($this->nodeInfo['type'] == 'single'){
            $view = M('Page'.ucfirst($this->nodeInfo['type']))->where('nid',$this->nid)->find();
            $this->assign('view', $view);
        }
        //得到当前节点所有子节点
        $arrId = getChildsId($this->category,$this->nid,true);
        //获取当前节点内容列表
        $where['nid'] = array('IN',$arrId);
        //搜素条件
        $get_keyword = trim(I('keyword'));
        $this->assign('keyword', $get_keyword);
        if($get_keyword != ''){
            $where['title'] = array('LIKE', '%'.$get_keyword.'%');
        }
        $get_start_time = str_replace('+',' ',trim(I('start_time')));
        $this->assign('start_time', $get_start_time);
        if($get_start_time != ''){
            $where['add_time'] = array('EGT',$get_start_time);
        }
        $get_end_time = str_replace('+',' ',trim(I('end_time')));
        $this->assign('end_time', $get_end_time);
        if($get_end_time != ''){
            $where['add_time'] = array('ELT',$get_end_time);
        }
        if($get_start_time != '' && $get_end_time != ''){
            $where['add_time'] = array(array('EGT',$get_start_time),array('ELT',$get_end_time));
        }
        $Model_Data = M('Page'.ucfirst($this->nodeInfo['type']));
        $list = $Model_Data->where($where)->order('id desc')->page($p.',12')->select();
        $count = $Model_Data->where($where)->count();
        $Pages = new \Think\Page($count, 12);
        $page = $Pages -> show();

        $this->assign('list', $list);
        $this->assign('page', $page);

        $this->display();
    }
    // 新增
    public function insert(){
        $this->display($this->nodeInfo['type']);
    }
    public function add(){
        if(IS_AJAX){
            $Model_Data = M('Page'.ucfirst($this->nodeInfo['type']));
            if($Model_Data->create()){
                $Model_Data->nid = $this->nid;
                $Model_Data->author_id = session('admin_id');
                $Model_Data->create_time = I('create_time') ? I('create_time') : date("Y-m-d H:i:s");
                $Model_Data->is_status = I('is_status') != 1 ? 0 : 1;
                $Model_Data->is_recommend = I('is_recommend') != 1 ? 0 : 1;
                $Model_Data->add() !== false ? $this->success('新增成功!',U('index').'?spm='.$this->spm,'page') : $this->error('新增失败!');
            }else{
                $this->error($Model_Data->getError());
            }
        }else{
            echo "非法操作";
        }
    }
    // 编辑
    public function update(){
        $view = M('Page'.ucfirst($this->nodeInfo['type']))->where(array('id'=>I('get.id')))->find();
        $this->assign('view', $view);
        $gallery = M('PageGallery')->where(array('gid'=>$view['gid']))->select();
        $this->assign('gallery', $gallery);

        $this->display($this->nodeInfo['type']);
    }
    public function save(){
        if(IS_AJAX){
            $Model_Data = M('Page'.ucfirst($this->nodeInfo['type']));
            if($Model_Data->create()){
                $Model_Data->add_time = I('add_time') ? I('add_time') : date("Y-m-d H:i:s");
                $Model_Data->is_status = I('is_status') != 1 ? 0 : 1;
                $Model_Data->is_recommend = I('is_recommend') != 1 ? 0 : 1;
                $Model_Data->save() !== false ? $this->success('保存成功!',U('index').'?spm='.$this->spm,'page') : $this->error('保存失败!');
            }else{
                $this->error($Model_Data->getError());
            }
        }else{
            echo "非法操作";
        }
    }
    public function edit(){
        if(IS_AJAX){
            $Model_Data = M('Page'.ucfirst($this->nodeInfo['type']));
            if($Model_Data->create()){
                $Model_Data->save() !== false ? $this->success('保存成功!',U('index').'?spm='.$this->spm,'page') : $this->error($Model_Data->getLastSql());
            }else{
                $this->error($Model_Data->getError());
            }
        }else{
            return "非法操作";
        }
    }
    // 删除
    public function delete(){
        if(IS_AJAX){
            $Model_Data = M('Page'.ucfirst($this->nodeInfo['type']));
            $id = I('get.id') ? I('get.id') : I('id');
            if(is_array($id)){
                foreach($id as $value){
                    $Data = $Model_Data -> where(array('id' => $value)) -> find();
                    @unlink('.'.$Data['thumb']);
                    galleryDelete($Data['gid']);
                }
            }else{
                $Data = $Model_Data -> where(array('id'=>$id)) -> find();
    			@unlink('.'.$Data['thumb']);
    			galleryDelete($Data['gid']);
            }
            $where['id'] = array('IN',$id);
            $Model_Data->where($where)->delete() !== false ? $this->success("删除成功!",U('index').'?spm='.$this->spm) : $this->error('删除失败!');
        }else{
            echo "非法操作";
        }
    }
    // 移动
    public function mobile(){
        if(IS_AJAX){
            $Model_Data = M('Page'.ucfirst($this->nodeInfo['type']));
            $id = I('id');
            $nid = I('news_nid');
            if($id){
                foreach($id as $value){
                    $data = array('nid'=>$nid);
                    $Model_Data->where(array('id'=>$value))->setField($data);
                }
                $this->success('移动成功！',U('index').'?spm='.$this->spm);
            }else{
                $this->error('移动失败！');
            }

        }else{
            echo "非法操作";
        }
    }
}
